import { Component, OnInit, ViewChild } from '@angular/core';
import { TodoService } from '../todo.service';
import { TodoItem } from '../models/TodoItem';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {
  @ViewChild('toDoItem') todoInput;
  private readonly _todoList: TodoItem[] = this.todoService.todoList;
  btnDisabled = true;

  constructor(private todoService: TodoService) { }

  ngOnInit() {
    
  }

  onAddToDo(todoName) {
    const itemAlreadyExists = this._todoList
      .findIndex((el) => el.name === todoName.trim())

    if(todoName.trim().length > 3 && itemAlreadyExists === -1){
      const todoItem = new TodoItem(todoName);
      this.todoService.addTodoItem(todoItem);
    }
  }
}
