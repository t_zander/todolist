import { Injectable } from '@angular/core';
import { TodoItem } from './models/TodoItem';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private _parcedStorageList = JSON.parse(localStorage.getItem('todoList'));
  private _todoList = this._parcedStorageList ? this._parcedStorageList : [];

  get todoList() {
    return this._todoList;
  }

  constructor() { }

  addTodoItem(item: TodoItem): void {  
    this._todoList.push(item);
    this.updateLocalStorage();
  }

  editItem(i: number, item: TodoItem): void {
    this._todoList[i] = item;
    this.updateLocalStorage();
  }

  deleteItem(i: number): void {
    this._todoList.splice(i, 1);
    this.updateLocalStorage();
  }

  private updateLocalStorage(): void {
    localStorage.setItem('todoList', JSON.stringify(this._todoList))
  }
}
