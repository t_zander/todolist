import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { TodoService } from '../todo.service';
import { TodoItem } from '../models/TodoItem';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  todoList: TodoItem[];
  currentEditable: number;
  editedItem: TodoItem;
  
  constructor(private todoService: TodoService) { }

  ngOnInit() {
    this.todoList = this.todoService.todoList;
  }

  todoDone(i: number, done: boolean) {
    this.todoList[i].done = done;
    this.todoService.editItem(i, this.todoList[i]);
  }

  onEditItem(i, item) {
    this.currentEditable = i;
    this.editedItem = item;
  }

  onSaveChanges(i) {
    this.todoService.editItem(i, this.editedItem);
    this.currentEditable = null;
  }

  onDeleteItem(i) {
    this.todoService.deleteItem(i);
  }
}
